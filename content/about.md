+++
title = "About"
date = "2024-2-3"
aliases = ["about-me", "contact", "cv", "resume", "hakkimda", "ozgecmis"]
[author]
  name = "Berkay Gündüz"
[params.about]
  enableThemeToggle = true
  [params.about.logo]
    logoText = "$ cd /home/about"
+++

---


Junior [CS](https://w3.cs.bilkent.edu.tr/en/) student @ [Bilkent University](https://w3.bilkent.edu.tr/bilkent/) 🎓

Interested in all about computers 💻

Lifelong learner 🎯

---

### Education

- **B.S. Computer Science**
  - Bilkent University, Sep 2021 - Jun 2025
  - CGPA: 3.13/4.00
  - High Honor Student for 2021-2022
  - Graduated as valedictorian from high school

---

### Experience

- **Software Engineer Intern**
  - Cape of Good Code GmbH, Jan 2024 - Present
  - Developing software analysis solutions for the DETANGLE Analysis Suite
  - Technologies: Python, Django, Vue.js, Git, Jira

- **Software Developer**
  - Nart Bilişim Hizmetleri, Aug 2023 - Dec 2023
  - Developed full-stack solutions for the STAR project (Turkcell)
  - Technologies: Python, Django, REST API, SOAP API, MySQL, JavaScript, jQuery, Git, Linux

- **Backend Developer Intern**
  - Nart Bilişim Hizmetleri, Jun 2023 - Aug 2023
  - Developed a network discovery script and solutions for major issues
  - Technologies: Python, Django, SOAP API, Git, Linux

---

### Projects

- **[geddit](https://github.com/berkaygunduzz/geddit)**
  - A Django API service to wrap Reddit posts

- **Moondark**
  - Aims to generate music like Beethoven's Moonlight Sonata and Erik Satie's Gymnopédie 1 with LSTM using Python 3 and TensorFlow

- **Lucid Dreams**
  - A 2D platformer game with a custom-level editor using Java and PostgreSQL

- **Ozan**
  - A collection of projects aiming to create songs like Turkish folk musician Aşık Veysel with artificial neural networks using Python for TÜBITAK 2204-A High School Students Research Students Competition

---

### Volunteering

- **Bilkent University, Webmaster & Lab Tutor**
  - Sep 2022 - Present
  - Maintenance of WordPress websites and lab tutor for various courses

- **IEEE Bilkent Student Branch, Chapter Chair**
  - Jun 2022 - Jun 2023
  - Led various organizations with 500+ participants, including sessions with tech companies, programming contests, meetings with alums, and Python and Java programming tutorials

---

### Skills

- Python, Java, C/C++, TypeScript
- Fast API, Django, Vue.js, TensorFlow, MySQL, Git, Bash Script, Docker
- Linux: Ubuntu, OpenSUSE, RedHat 
- Full-stack development, OOP, software analysis, deep learning, and applications

---

### Certificates

- [TensorFlow Developer Certificate](https://www.credential.net/750a4b7f-38f2-42ea-b10f-af161e67103f) - Nov 2022
- [Deep Learning Specialization](https://www.coursera.org/verify/specialization/N4BRUUBUU4R7) - Oct 2022
- Mustafa Akgül Free Software Summer Camp Python 3 Programming - Aug 2019

---

### Languages

- Turkish (native)
- English (professional working proficiency)
- German (introductory)

---

### Activities

- **Google Developers Machine Learning Bootcamp** - Oct 2022
  - Participant
  - Learned fundamentals of neural networks, machine learning, deep learning, convolutional neural networks, natural language processing, and RNN, and programming deep learning solutions using Python and TensorFlow

- **Google Code-In** - Dec 2019 - Jan 2020
  - Participant Developer
  - Completed several tasks given by Fedora Project and Copyleft Games using skills in Python programming and Linux

- **TÜBİTAK 2204-A** - Mar 2020
  - 3rd Rank Regional third degree at TÜBİTAK (The Scientific and Technical Research Council of Turkey) 2204-A High School Students Research Students Competition

---

### Hobbies & Interests

- Photography, cinema, nature, trekking, history, politics, philosophy
